class Controls extends Phaser.Scene {
	constructor() {
		super("Controls");
	}

	create() {
		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;
		this.add.image(0, 0, 'mainBackground').setOrigin(0, 0).setDisplaySize(this.cameras.main.width, this.cameras.main.height).setInteractive().on('pointerup', () => this.scene.start('Menu'));;
		var ctrls = this.add.image(0.5 * this.width, 0.5 * this.height, 'controls');
		ctrls.setScale(this.height / ctrls.height);
	}

	update() {
		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;
	}

}

export default Controls;