import Menu from './menu.js'

class BasaCutscene extends Phaser.Scene {
	constructor() {
		super('BasaCutscene');
	}

	create () {
 		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;
		
		try {
			this.vid = this.add.video(this.width/2, this.height / 2, 'basaCutscene');
			this.vid.setScale(this.height / this.vid.height);
			this.vid.video.setAttribute('webkit-playsinline', 'webkit-playsinline');
			this.vid.video.setAttribute('playsinline', 'playsinline');
			this.vid.play();
			this.starterPlaying = false;
		} catch (e) {
			console.error("Not able to start basa cutscene. Treska smutna, skipping.");
			this.scene.start('Menu');
		}
	}

	update() {
		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;
	
		if(this.startedPlaying && !this.vid.isPlaying()) {
			this.scene.start('Menu');
		}
		if(!this.vid.isPlaying()) {
			this.startedPlaying = true;
		}
	}
}

export default BasaCutscene;