// import Marimba from "./levels/marimba.js"

class Menu extends Phaser.Scene {
	constructor () {
		super("Menu");
	}

	create() {
 		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;

		var marimbaDone = localStorage.getItem('marimbaDone') || 'false';
		var enjoyDone = localStorage.getItem('enjoyDone') || 'false';
		var kainDone = localStorage.getItem('kainDone') || 'false';
		var bigfatDone = localStorage.getItem('bigfatDone') || 'false';

		this.totalScore = 0;

		if(marimbaDone == 'true') {
			this.totalScore++;
		}
		if(enjoyDone == 'true') {
			this.totalScore++;
		}
		if(kainDone == 'true') {
			this.totalScore++;
		}
		if(bigfatDone == 'true') {
			this.totalScore++;
		}

		this.add.image(0, 0, 'menuBackground').setOrigin(0, 0).setDisplaySize(this.cameras.main.width, this.cameras.main.height);
		this.songName = this.add.image(0.02 * this.width, 0.035 * this.height, 'pinkStafls-text').setOrigin(0, 0);
		this.songName.setScale(0.15 * this.height / this.songName.height);

		this.replayButton = this.add.image(0.55 * this.width, 0.1 * this.height, 'replayButton').setInteractive();
		this.replayButton.setScale(0.15 * this.height / this.replayButton.height);

		this.replayButton.on('pointerover', () => this.replayButton.setScale(0.17 * this.height / this.replayButton.height));
		this.replayButton.on('pointerout', () => this.replayButton.setScale(0.15 * this.height / this.replayButton.height));
		this.replayButton.on('pointerup', () => this.scene.start('Cutscene1'));

		this.controlsButton = this.add.image(0.65 * this.width, 0.1 * this.height, 'controlsButton').setInteractive();
		this.controlsButton.setScale(0.15 * this.height / this.controlsButton.height);

		this.controlsButton.on('pointerover', () => this.controlsButton.setScale(0.17 * this.height / this.controlsButton.height));
		this.controlsButton.on('pointerout', () => this.controlsButton.setScale(0.15 * this.height / this.controlsButton.height));
		this.controlsButton.on('pointerup', () => this.scene.start("Controls"));

		this.beerStatus = this.add.image(0, 0, 'beer' + this.totalScore).setOrigin(0, 0);
		this.beerStatus.setScale(this.height / this.beerStatus.height);

		var lvl1 = this.add.image(this.width * 0.288, this.height * 0.3796, 'prev1').setInteractive();
		lvl1.setScale(0.3 * this.width / lvl1.width);
		lvl1.on('pointerover', () => {
			this.enlargeLevel(lvl1);
			this.songName.setTexture('marimba-text');
			this.songName.setScale(0.15 * this.height / this.songName.height);
		});
		lvl1.on('pointerout', () => this.reduceLevel(lvl1));
		lvl1.on('pointerup', () => this.scene.start('Marimba'));
		if(marimbaDone == 'true') {
			this.add.image(this.width * 0.4, this.height * 0.5, 'tick').setScale(0.25 * this.width / lvl1.width);
		}

		
		var lvl2 = this.add.image(this.width * 0.712, this.height * 0.3796, 'pinkLock').setInteractive();
		lvl2.setScale(0.1 * this.width / lvl2.width);
		lvl2.on('pointerover', () => {
			this.enlargeLock(lvl2);
			this.songName.setTexture('enjoy-text');
			this.songName.setScale(0.13 * this.height / this.songName.height);
		});
		lvl2.on('pointerout', () => this.reduceLock(lvl2));
		// lvl2.on('pointerup', () => this.scene.start('Marimba'));

		var lvl3 = this.add.image(this.width * 0.288, this.height * 0.814, 'pinkLock').setInteractive();
		lvl3.setScale(0.1 * this.width / lvl3.width);
		lvl3.on('pointerover', () => {
			this.enlargeLock(lvl3);
			this.songName.setTexture('kain-text');
			this.songName.setScale(0.15 * this.height / this.songName.height);
		});
		lvl3.on('pointerout', () => this.reduceLock(lvl3));
		// lvl3.on('pointerup', () => this.scene.start('Marimba'));

		var lvl4 = this.add.image(this.width * 0.712, this.height * 0.814, 'pinkLock').setInteractive();
		lvl4.setScale(0.1 * this.width / lvl4.width);
		lvl4.on('pointerover', () => {
			this.enlargeLock(lvl4);
			this.songName.setTexture('bigfat-text');
			this.songName.setScale(0.15 * this.height / this.songName.height);
		});
		lvl4.on('pointerout', () => this.reduceLock(lvl4));
		// lvl4.on('pointerup', () => this.scene.start('Marimba'));
	}

	enlargeLevel(lvl) {
		lvl.setScale(0.32 * this.width / lvl.width);
	}

	reduceLevel(lvl) {
		lvl.setScale(0.3 * this.width / lvl.width);
	}


	enlargeLock(lvl) {
		lvl.setScale(0.12 * this.width / lvl.width);
	}

	reduceLock(lvl) {
		lvl.setScale(0.1 * this.width / lvl.width);
	}

	addPack() {
		this.totalScore += 1;
	}
}

export default Menu;