import Menu from '../menu.js'

class Marimba extends Phaser.Scene {

    constructor () {
        super('Marimba');
    }

    create ()
    {   
        // Init
        this.width = this.cameras.main.width;
        this.height = this.cameras.main.height;
        this.mapWidth = 10000 * (this.height / 700);

        this.physics.world.gravity.y = 1000 * (this.height / 700);
        this.physics.world.setBounds(0, 0,  this.mapWidth, this.height);
        this.cameras.main.setBounds(0, 0, this.mapWidth, this.height);

        this.score = 0;
        this.scoreBoard = this.add.image();

        // Sound
        this.sound.pauseOnBlur = false;
        
        var soundtrack = this.sound.add('marimba', {loop: true});
        this.walkSound = this.sound.add('walkSound', {loop: true});
        this.walkSound.play();
        this.walkSound.pause();
        soundtrack.play();

        // Textures
        this.background = this.add.image(0, 0, 'marimbaBackground').setOrigin(0, 0).setScale(this.height / 490);
        this.background.scrollFactorX = 0.8;

        this.add.image(0, this.height / 12, 'marimbaGround').setOrigin(0, 0).setScale(this.height / 490);
        this.add.image(0, 0, 'marimbaObstacles').setOrigin(0, 0).setScale(this.height / 700);

        // Platforms
        this.createPlatforms();

        //Pivko
        this.createPivka();

        this.songName = this.add.image(0.02 * this.width, 0.035 * this.height, 'marimba-text').setOrigin(0, 0);
        this.songName.setScale(0.15 * this.height / this.songName.height);
        this.songName.scrollFactorX = 0;

        this.scoreImage = this.add.image(0.88 * this.width, 0, 'scoreBeer' + this.score).setOrigin(0, 0);
        this.scoreImage.setScale(0.17 * this.height / this.scoreImage.height);
        this.scoreImage.scrollFactorX = 0;

        // Player
        this.createPlayer();


        // Controls
        this.initControls();
        
        this.physics.add.collider(this.player, this.platforms);
        this.physics.add.overlap(this.player, this.beers, this.collectBeer, null, this);
    }

    initControls() {
        var defaultAplha = 0.7;
        var touchAlpha = 1;
        var arrowHeight = 0.725;

        this.arrowGoingLeft = false;
        this.arrowGoingRight = false;
        this.arrowJumping = false;

        if(this.sys.game.device.os.desktop) {
            return;
        }

        this.leftArrow = this.add.image(0, this.height * arrowHeight, 'arrowLeft').setOrigin(0, 0).setInteractive().setAlpha(defaultAplha);
        this.leftArrow.scrollFactorX = 0;
        this.leftArrow.on('pointerdown', () => {
            this.arrowGoingLeft = true;
            this.leftArrow.setAlpha(touchAlpha);
        });
        this.leftArrow.on('pointerup', () => {
            this.arrowGoingLeft = false;
            this.leftArrow.setAlpha(defaultAplha);
        });

        var arrowScale = 0.25 * this.height / this.leftArrow.height;
        this.leftArrow.setScale(arrowScale);

        this.rightArrow = this.add.image(this.leftArrow.width * arrowScale * 1.1, this.height * arrowHeight, 'arrowRight').setOrigin(0, 0).setInteractive().setAlpha(defaultAplha);
        this.rightArrow.scrollFactorX = 0;
        this.rightArrow.on('pointerdown', () => {
            this.arrowGoingRight = true;
            this.rightArrow.setAlpha(touchAlpha);
        });
        this.rightArrow.on('pointerup', () => {
            this.arrowGoingRight = false;
            this.rightArrow.setAlpha(defaultAplha);
        });
        this.rightArrow.setScale(arrowScale);

        this.upArrow = this.add.image(this.width * 0.85, this.height * arrowHeight, 'arrowUp').setOrigin(0, 0).setInteractive().setAlpha(defaultAplha);
        this.upArrow.scrollFactorX = 0;
        this.upArrow.on('pointerdown', () => {
            this.arrowJumping = true;
            this.upArrow.setAlpha(touchAlpha);
        });

        this.upArrow.on('pointerup', () => {
            this.arrowJumping = false;
            this.upArrow.setAlpha(defaultAplha);
        });
        this.upArrow.setScale(arrowScale);
    }

    createPivka() {
        this.beers = this.physics.add.staticGroup();
        // Ground

        var beerHeight = this.add.image(-1000, -1000, 'pivko').height;
        this.beers.create(853 * (this.mapWidth / 10000), 373 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(1698 * (this.mapWidth / 10000), 70 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(2648 * (this.mapWidth / 10000), 392 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(3436 * (this.mapWidth / 10000), 264 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(4884 * (this.mapWidth / 10000), 188 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(5732 * (this.mapWidth / 10000), 372 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(5952 * (this.mapWidth / 10000), 128 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(7272 * (this.mapWidth / 10000), 132 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(8648 * (this.mapWidth / 10000), 204 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();
        this.beers.create(9504 * (this.mapWidth / 10000), 140 * (this.height / 700), 'pivko').setScale(0.15 * this.height / beerHeight).refreshBody();

    }

    createPlatforms() {
        this.platforms = this.physics.add.staticGroup();
        // Ground
        this.platforms.create(0, this.height * 0.95, 'obstacle').setOrigin(0, 0).setDisplaySize(this.mapWidth, this.height * 0.05).refreshBody();
    
        this.platforms.create(773 * (this.mapWidth / 10000), 443 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(171 * (this.mapWidth / 10000), 22 * (this.height / 700)).refreshBody();
        this.platforms.create(1275 * (this.mapWidth / 10000), 438 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(193 * (this.mapWidth / 10000), 22 * (this.height / 700)).refreshBody();
        this.platforms.create(1595 * (this.mapWidth / 10000), 326 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(182 * (this.mapWidth / 10000), 27 * (this.height / 700)).refreshBody();
        this.platforms.create(2555 * (this.mapWidth / 10000), 532 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(166 * (this.mapWidth / 10000), 26 * (this.height / 700)).refreshBody();
        this.platforms.create(2862 * (this.mapWidth / 10000), 402 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(150 * (this.mapWidth / 10000), 22 * (this.height / 700)).refreshBody();
        this.platforms.create(3158 * (this.mapWidth / 10000), 252 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(179 * (this.mapWidth / 10000), 22 * (this.height / 700)).refreshBody();
        this.platforms.create(3320 * (this.mapWidth / 10000), 334 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(224 * (this.mapWidth / 10000), 28 * (this.height / 700)).refreshBody();
        this.platforms.create(4270 * (this.mapWidth / 10000), 459 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(183 * (this.mapWidth / 10000), 26 * (this.height / 700)).refreshBody();
        this.platforms.create(4431 * (this.mapWidth / 10000), 368 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(181 * (this.mapWidth / 10000), 30 * (this.height / 700)).refreshBody();
        this.platforms.create(5150 * (this.mapWidth / 10000), 469 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(178 * (this.mapWidth / 10000), 27 * (this.height / 700)).refreshBody();
        this.platforms.create(5459 * (this.mapWidth / 10000), 353 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(169 * (this.mapWidth / 10000), 20 * (this.height / 700)).refreshBody();
        this.platforms.create(5607 * (this.mapWidth / 10000), 453 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(262 * (this.mapWidth / 10000), 29 * (this.height / 700)).refreshBody();
        this.platforms.create(5840 * (this.mapWidth / 10000), 248 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(15 * (this.mapWidth / 10000), 222 * (this.height / 700)).refreshBody();
        this.platforms.create(5840 * (this.mapWidth / 10000), 216 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(203 * (this.mapWidth / 10000), 29 * (this.height / 700)).refreshBody();
        this.platforms.create(6641 * (this.mapWidth / 10000), 482 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(210 * (this.mapWidth / 10000), 25 * (this.height / 700)).refreshBody();
        this.platforms.create(7169 * (this.mapWidth / 10000), 465 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(178 * (this.mapWidth / 10000), 27 * (this.height / 700)).refreshBody();
        //arrow
        this.platforms.create(7850 * (this.mapWidth / 10000), 465 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(183 * (this.mapWidth / 10000), 24 * (this.height / 700)).refreshBody();
        this.platforms.create(8220 * (this.mapWidth / 10000), 327 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(194 * (this.mapWidth / 10000), 26 * (this.height / 700)).refreshBody();
        this.platforms.create(8562 * (this.mapWidth / 10000), 320 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(194 * (this.mapWidth / 10000), 22 * (this.height / 700)).refreshBody();
        this.platforms.create(8930 * (this.mapWidth / 10000), 315 * (this.height / 700), 'obstacle').setOrigin(0, 0).setDisplaySize(190 * (this.mapWidth / 10000), 22 * (this.height / 700)).refreshBody();            
    }   

    createPlayer() {        
        var spriteScale = (this.height / 4) / 350;
        // scales the whole sprite
        this.player = this.physics.add.sprite(this.mapWidth * 0.01, this.width * 0.9, 'treska').setScale(spriteScale);
        // sets bounding box size
        this.player.setSize(this.player.width * 0.5, this.player.width * 0.9);

        this.player.setCollideWorldBounds(true);

        this.camera = this.cameras.main;

        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('treska', { start: 12, end: 0 }),
            frameRate: 25,
            repeat: -1
        });

        this.anims.create({
            key: 'stand-right',
            frames: [ { key: 'treska', frame: 13 } ],
            frameRate: 20
        });

        this.anims.create({
            key: 'stand-left',
            frames: [ { key: 'treska', frame: 12 } ],
            frameRate: 20
        });

        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('treska', { start: 13, end: 25 }),
            frameRate: 30,
            repeat: -1
        });
    }

    collectBeer(player, pivko) {
        this.sound.play('beerSound');
        pivko.destroy();
        this.score += 1;
        this.scoreImage.setTexture('scoreBeer' + this.score);
    }

    update() 
    {
        this.width = this.cameras.main.width;
        this.height = this.cameras.main.height;
    
        this.cursors = this.input.keyboard.createCursorKeys();
        this.camera.startFollow(this.player);

        if(this.player.body.touching.down && this.score == 10 && this.player.body.velocity.x == 0) {
            // TODO separate to fc
            localStorage.setItem('marimbaDone', 'true');
            this.sound.stopAll();
            this.scene.start("BasaCutscene");
        }

        if (this.cursors.left.isDown || this.arrowGoingLeft) {
            this.player.setVelocityX(-400 * (this.height / 700));

            if(this.player.body.touching.down) {
                this.player.anims.play('left', true);
                if(this.walkSound.isPaused) {
                    this.walkSound.resume();
                }
            } else {
                this.player.anims.play('stand-left');
                this.walkSound.pause(); 
            }
        } else if (this.cursors.right.isDown || this.arrowGoingRight) {
            this.player.setVelocityX(400 * (this.height / 700));
            if(this.player.body.touching.down) {
                this.player.anims.play('right', true);
                if(this.walkSound.isPaused) {
                    this.walkSound.resume();
                }
            } else {
                this.player.anims.play('stand-right');
                this.walkSound.pause(); 
            }
        } else {
            if(this.player.body.velocity.x > 0) {
                this.player.anims.play('stand-right');
            } else if (this.player.body.velocity.x < 0) {
                this.player.anims.play('stand-left');    
            }
            this.walkSound.pause(); 
            this.player.setVelocityX(0);
        }

        if ((this.cursors.up.isDown || this.arrowJumping) && this.player.body.touching.down)
        {
            this.sound.play('jumpSound');
            this.player.setVelocityY(-680 * (this.height / 700));
        }
        // Pridani rychlosti padani pri drzeni sipky dolu
        if (this.cursors.down.isDown && !this.player.body.touching.down)
        {
            this.player.body.velocity.y += 60;
        }

    }

}

export default Marimba;