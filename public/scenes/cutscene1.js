import Menu from './menu.js'

class Cutscene1 extends Phaser.Scene {
	constructor() {
		super('Cutscene1');
	}

	create () {
 		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;
		
		try {
			this.vid = this.add.video(this.width/2, this.height / 2, 'cutscene1');
			this.vid.setScale(this.height / this.vid.height);
			this.vid.video.setAttribute('webkit-playsinline', 'webkit-playsinline');
			this.vid.video.setAttribute('playsinline', 'playsinline');
			this.vid.play();
			this.starterPlaying = false;
		} catch (e) {
			console.error("Not able to start cutscene 1. Skipping")
			this.scene.start('Menu');
		}


		var skipButton = this.add.image(0.92 * this.width, 0.9 * this.height, 'skipButton').setInteractive();
		skipButton.setScale(0.1 * this.width / skipButton.width);
		skipButton.on('pointerover', () => skipButton.setScale(0.15 * this.width / skipButton.width));
		skipButton.on('pointerout', () => skipButton.setScale(0.1 * this.width / skipButton.width));
		skipButton.on('pointerup', () => this.scene.start('Menu'));
	}

	update() {
		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;
	
		if(this.startedPlaying && !this.vid.isPlaying()) {
			this.scene.start('Menu');
		}
		if(!this.vid.isPlaying()) {
			this.startedPlaying = true;
		}
	}
}

export default Cutscene1;