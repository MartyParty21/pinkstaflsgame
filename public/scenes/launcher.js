import Cutscene1 from './cutscene1.js'
import BasaCutscene from './basaCutscene.js'
import Menu from './menu.js'
import Marimba from './levels/marimba.js'
import Controls from './controls.js'

class Launcher extends Phaser.Scene {
	
	constructor () {
		super('Launcher');
	}

	preload () {	
        var width = this.cameras.main.width;
        var height = this.cameras.main.height;

     	var progressBar = this.add.graphics();
        
        var loadingText = this.make.text({
            x: width / 2,
            y: height / 2 - 50,
            text: 'Načítám zábavu...',
            style: {
                font: '20px monospace',
                fill: '#ffffff'
            }
        });
        loadingText.setOrigin(0.5, 0.5);
        
        var percentText = this.make.text({
            x: width / 2,
            y: height / 2 - 5,
            text: '0%',
            style: {
                font: '18px monospace',
                fill: '#ffffff'
            }
        });
        percentText.setOrigin(0.5, 0.5);
        
        var assetText = this.make.text({
            x: width / 2,
            y: height / 2 + 50,
            text: '',
            style: {
                font: '18px monospace',
                fill: '#ffffff'
            }
        });
        assetText.setOrigin(0.5, 0.5);
        
        this.load.on('progress', function (value) {
            percentText.setText(parseInt(value * 100) + '%');
            progressBar.clear();
            progressBar.fillStyle(0xffffff, 1);
            progressBar.fillRect(width / 2 - 150, height / 2 + 10, 300 * value, 30);
        });
        
        this.load.on('fileprogress', function (file) {
            assetText.setText(file.key);
        });
        this.load.on('complete', function () {
            progressBar.destroy();
            loadingText.destroy();
            percentText.destroy();
            assetText.destroy();
        });

		this.load.video('mainVid', 'assets/menu/mainVid.mp4');
		this.load.image('enterButton', 'assets/menu/enter.png');

		this.load.video('basaCutscene', 'assets/levels/treskaBasa.mp4');

		this.load.video('cutscene1', 'assets/menu/cutscene1.mp4');
		this.load.image('skipButton', 'assets/menu/skip.png');

		this.load.image('mainBackground', 'assets/menu/background.png');
		this.load.image('menuBackground', 'assets/menu/menu-background.png');
		this.load.image('controlsButton', 'assets/menu/bar-controls.png');
		this.load.image('replayButton', 'assets/menu/bar-replay.png');
		this.load.image('beer0', 'assets/menu/beer0.png');
		this.load.image('beer1', 'assets/menu/beer1.png');
		this.load.image('beer2', 'assets/menu/beer2.png');
		this.load.image('beer3', 'assets/menu/beer3.png');
		this.load.image('beer4', 'assets/menu/beer4.png');

		this.load.image('pivko', 'assets/levels/pivko.png');

		this.load.image('scoreBeer0', 'assets/inventory/0.png');
		this.load.image('scoreBeer1', 'assets/inventory/1.png');
		this.load.image('scoreBeer2', 'assets/inventory/2.png');
		this.load.image('scoreBeer3', 'assets/inventory/3.png');
		this.load.image('scoreBeer4', 'assets/inventory/4.png');
		this.load.image('scoreBeer5', 'assets/inventory/5.png');
		this.load.image('scoreBeer6', 'assets/inventory/6.png');
		this.load.image('scoreBeer7', 'assets/inventory/7.png');
		this.load.image('scoreBeer8', 'assets/inventory/8.png');
		this.load.image('scoreBeer9', 'assets/inventory/9.png');
		this.load.image('scoreBeer10', 'assets/inventory/10.png');

		this.load.image('prev1', 'assets/menu/prev1.png');
		this.load.image('pinkLock', 'assets/menu/lock.png');

		this.load.image('tick', 'assets/menu/green-tick.png');

		this.load.image('controls', 'assets/menu/controls.png');
		this.load.image('arrowLeft', 'assets/levels/arrow-left.png');
		this.load.image('arrowRight', 'assets/levels/arrow-right.png');
		this.load.image('arrowUp', 'assets/levels/arrow-up.png');


		this.load.image('pinkStafls-text', 'assets/menu/pinkStafls-text.png');
		this.load.image('marimba-text', 'assets/levels/marimba/marimba.png');
		this.load.image('kain-text', 'assets/levels/kain/kain.png');
		this.load.image('enjoy-text', 'assets/levels/enjoy/enjoy.png');
		this.load.image('bigfat-text', 'assets/levels/bigfat/bigfat.png');

    	this.load.spritesheet('treska', 'assets/sprites/treska-sprite.png', { frameWidth: 350, frameHeight: 350 });
    	this.load.image('obstacle', 'assets/levels/ghostObstacle.png');

    	this.load.audio('beerSound', 'assets/audio/beer-sound.wav');
    	this.load.audio('jumpSound', 'assets/audio/jump-sound.wav');
    	this.load.audio('walkSound', 'assets/audio/walk.mp3');

        this.load.image('marimbaBackground', 'assets/levels/marimba/background.png');
        this.load.image('marimbaGround', 'assets/levels/marimba/ground.png');
        this.load.image('marimbaObstacles', 'assets/levels/marimba/obstacles.png');
        this.load.audio('marimba', 'assets/levels/marimba/marimba.mp3'); 
	}

 	create() {
 		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;

		try {
			var vid = this.add.video(this.width/2, this.height / 2, 'mainVid', false, true);
			vid.setScale(this.height / vid.height);
			vid.video.setAttribute('webkit-playsinline', 'webkit-playsinline');
			vid.video.setAttribute('playsinline', 'playsinline');
			vid.play(true);
		} catch (e) {
			console.error("Not able to start launcher background video, not gonna show. Troska situation");
		}

		this.enterButton = this.add.image(this.width / 2, this.height * 0.75, 'enterButton').setInteractive();
		this.enterButton.setScale(0.35 * this.width / this.enterButton.width);
		this.enterButton.on('pointerover', () => this.enlargeEnter());
		this.enterButton.on('pointerout', () => this.reduceEnter());
		this.enterButton.on('pointerup', () => this.scene.start('Cutscene1'));

	}

	update () {
 		this.width = this.cameras.main.width;
		this.height = this.cameras.main.height;
	}

	enlargeEnter() {
		this.enterButton.setScale(0.4* this.width / this.enterButton.width);
	}

	reduceEnter() {
		this.enterButton.setScale(0.35 * this.width / this.enterButton.width);
	}
}


var realWidth = window.innerWidth;
var realHeight = window.innerHeight;
var prefWidth;
var prefHeight;


if(realWidth * ( 9 / 16) > realHeight) {
	prefHeight = realHeight;
	prefWidth = prefHeight * (16 / 9);
} else {
	prefWidth = realWidth;
	prefHeight = prefWidth * (9 / 16);
}

var config = {
    type: Phaser.AUTO,
    width: prefWidth,
    height: prefHeight,
    scene: [ Launcher, Cutscene1, BasaCutscene, Menu, Controls, Marimba ],
    scale: {
	    parent: 'gm',
        mode: Phaser.Scale.FIT,
	    autoCenter: Phaser.Scale.CENTER_HORIZONTALLY
  	},  
  	input :{
		activePointers:3,
	},


    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 1000 },
            debug: false
        }
    }
};

var game = new Phaser.Game(config);